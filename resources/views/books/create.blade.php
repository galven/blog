@extends('layouts.app')
@section('content')

<h1> Create a new book in the list </h1>

<form method = 'post' action = "{{action('BookController@store')}}">
{{csrf_field()}}

<ul>
@foreach($errors->all() as $error)
   <li>{{$error}}</li>
@endforeach
</ul>

<div class="form-group">
<label for = "title">What is the name of the Book that You Like To Add? </label>
<input type = "text" class = "form-control" name= "title">
<label for = "title">Who is the author? </label>
<input type = "text" class = "form-control" name= "author">
</div>

<div class = "form-group">
    <input type="submit" class="form-control" name="submit" value= "Save">
</div>
</form>
@endsection

