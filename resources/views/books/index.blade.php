<!DOCTYPE html>
<html>
@yield('content')
@extends('layouts.app')
@section('content')  
<head><h3>This is your Book List</h3></head>
<body>
<table>
    <tr>
     <th>ID</th>
     @can('manager')<th>Book Title</th>@endcan 
     @cannot('manager') <th>Book Title</th>@endcannot
     <th>Book Author</th>
     <th>CheckBox</th>
    </tr>

    @foreach($books as $book)
    <tr> 
        <td> {{$book->id}}</td>
        @can('manager')<td> <a href = "{{route('books.edit',$book->id)}}"> {{$book->title}} </a></td>@endcan
        @cannot('manager') <td>  {{$book->title}} </td> @endcannot
        @can('manager')<td> <a href = "{{route('books.edit',$book->id)}}"> {{$book->author}} </a></td>@endcan
        @cannot('manager') <td> {{$book->author}} </td> @endcannot
      <td> @if ($book->status)
           <input type = 'checkbox' id ="{{$book->id}}" checked>
       @else
           <input type = 'checkbox' id ="{{$book->id}}">
       @endif </td>
   </tr>
   @endforeach
</table>
@can('manager') <a href = "{{route('books.create')}}"> create a new book </a> @endcan
<script>
       $(document).ready(function(){
           $(":checkbox").click(function(event){
               $.ajax({
                   url:"{{url('books')}}" + '/' + event.target.id,
                   dataType: 'json',
                   type: 'put',
                   contentType: 'application/json',
                   data:JSON.stringify({'status':event.target.checked, _token:'{{csrf_token()}}'}),
                   processData:false,
                   success: function(data){
                        console.log(JSON.stringify(data));
                   },
                   error: function(errorThrown){
                       console.log( errorThrown );
                   }
               });               
           });
       });
   </script>  

</body>
@endsection
</html>