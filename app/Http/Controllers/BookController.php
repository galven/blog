<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Book;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Http\Requests\StoreBookPost;

class BookController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()

    {
       // $id = Auth::id(); 
     //   $books = User::find($id)->books;
      //  return view('books.index',['books'=>$books]);
       $id = Auth::id();
       if (Gate::denies('manager')) {
            $boss = DB::table('employees')->where('employee',$id)->first();
            $id = $boss-> manager;
        }
        $user = User::find($id);
        $books = $user->books;
        return view('books.index', compact('books'));
 
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (Gate::denies('manager')) {
            abort(403,"Sorry you are not allowed to create books");
        }
 
       return view('books.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreBookPost $request)
    {
        if (Gate::denies('manager')) {
            abort(403,"Are you a hacker or what?");
       } 
       
     //  $validated = $request->validated();
       
       $books = new Book();
        $id = Auth::id();
        $books->title = $request->title;
        $books->user_id = $id;
        $books->author = $request->author;
        $books->status = 0;
        $books-> save();
        return redirect('books');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (Gate::denies('manager')) {
            abort(403,"Are you a hacker or what?"); // ניסיון פריצה
       }
        $book = Book::find($id);
        return view('books.edit', compact('book'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $book = Book::findOrFail($id);
        if (Gate::denies('manager')) {
            if ($request->has('title'))
                   abort(403,"You are not allowed to edit books.."); //עובדים לא רשאיים לשנות ספרים
        } 

    if (Gate::allows('manager')){   
       if(!$book->user->id == Auth::id()) return(redirect('books')); //בדיקה שאכן מי שמחובר לסשן הוא זה שמבצע עדכון
        $book->update($request-> except(['_token'])); // עדכון הצ'קבוקס
        if($request->ajax()){
            return Response::json(array('result' =>'sucess','status' =>$request->status),200);
        }
    }
    if (Gate::denies('employee')){
        if ($book->status == 0){ // אם הצ'קבוקס כרגע לא מסומן בוי
            if(!$book->user->id == Auth::id()) return(redirect('books')); // בדיקה שאכן מי שמחובר לסשן הוא זה שמבצע עדכון
        $book->update($request-> except(['_token'])); // עדכון הצ'קבוקס
                  if($request->ajax()){
                 return Response::json(array('result' =>'sucess','status' =>$request->status),200);
                 }
        }
    }
      return redirect('books');
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $book = Book::findOrFail($id);
        if (Gate::denies('manager')) {
          abort(403,"Are you a hacker or what?"); // נסיון פריצה
       }
        if(!$book->user->id == Auth::id()) return redirect('books');  // אבטחת מידע
        $book = Book::find($id);
        $book-> delete();
        return redirect('books');
    }
}
