<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class book extends Model
{
    // database fields to be allowed for massivr assignment
    protected $fillable = [
        'title', 'status'
    ];
    public function user(){

        return $this -> belongsTo('App\User');
    }
}
