<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
## תרגול בכיתה 
Route::get('/hello', function () {
    return "Hello world!";
});

Route::get('/student/{id?}', function ($id=" no student provided ") {
    return "Hello student " .$id;
})->name('students');

Route::get('/comment/{id}', function ($id) {
    return view('comment',['id'=>$id]);
})->name('comments');
## תרגיל מספר 1 
Route::get('/customer/{num?}', function ($num=null) {
    if ($num!= null) {
    return view('customernum', ['num'=>$num]);
}
    else 
    return view('nocustomernum');

})->name('customers');

Route:: resource('books','BookController')->middleware('auth');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');


