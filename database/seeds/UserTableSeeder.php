<?php

use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert(
            [
                [
                'name'=> 'Gal',
                'email'=>'ABC@gmail.com',
                'password' => '555555',
                'created_at' => date('y-m-d G:i:s'),
                ],
                [
                'name'=> 'Mor',
                'email'=>'EFG@jack.com',
                'password' => '12345678',
                'created_at' => date('y-m-d G:i:s'),
                ],
            ]
    );
    }
}